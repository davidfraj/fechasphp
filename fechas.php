<?php  

//Trabajaremos con funciones de fecha y hora en PHP
	//Funcion time() -> Numero de segundos transcurridos desde el 1 de enero de 1970 a las 00:00:00, hasta la fecha que queremos representar
	$hoy=time(); //Devuelve una fecha TIMESTAMP de ahora
	echo $hoy;
	echo '<br>';
	//Funcion mktime -> Permite generar la fecha TIMESTAMP que yo quiera
	$fecha=mktime(22,50,40,7,2,1990); // 2/7/1990 22:50:40
	echo $fecha;
	echo '<br>';
	//function Date, que sirve para representar FECHAS
	echo date('d/m/Y H:i:s', $fecha);
	echo '<br>';
	echo date('l', $fecha);
	echo '<br>';

	$diasSemana=array('domingo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado');
	echo $diasSemana[date('w',$fecha)];
	echo '<br>';

	$meses=array('', 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
	echo $meses[date('n',$fecha)];
	echo '<hr>';

	//Hoy es jueves, 27 de abril de 2017
	$saludo='Hoy es '.$diasSemana[date('w',time())].', '.date('d', time()).' de '.$meses[date('n',time())].' de '.date('Y', time());
	echo $saludo;

	echo $saludo;
?>




